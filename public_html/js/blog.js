$(function () {
    var APPLICATION_ID = "64762072-47A0-F818-FF28-0DB72066B100",
        SECRET_KEY = "73552AAA-7144-50C8-FF07-262222D4F600",
        VERSION = "v1";
        
      Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
     
     
      var postsCollection = Backendless.Persistence.of(Posts).find();
     
       console.log(postsCollection); 
      
      var wrapper = {
        posts:postsCollection.data  
      };
      
      Handlebars.registerHelper('format', function (time) {
          return moment(time).format("dddd, MMMM Do YYYY");   
      });
      
      var blogScript = $("#blogs-template").html();
      var blogTemplate = Handlebars.compile(blogScript);
      var blogHTML = blogTemplate(wrapper);
      
      $('.main-container').html(blogHTML);
      
});

  
function Posts(args){
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}

